#include <iostream>
#include "firmata/client.hpp"
#include "firmata/serial_port.hpp"

using namespace firmata::literals;
using namespace std::chrono_literals;

using namespace std;

const uint8_t PIN = 13;
const uint8_t IN = 12;

asio::io_context ioc;
boost::asio::steady_timer timer(ioc);
firmata::client *client;

void timer_handler(const boost::system::error_code& e)
{
  cout << client->pin(IN).state() << endl;
  timer.expires_after(boost::asio::chrono::seconds(1));
  timer.async_wait(timer_handler);
}

int main()
{
    cout << "main: start" << endl;
    firmata::serial_port device(ioc, "/dev/ttyACM0");
    device.set(115200_baud);
    firmata::client arduino(device);
    client = &arduino;
    cout << "arduino object created." << endl;
    arduino.pin(PIN).mode(mode::digital_out);
    for (int i = 3; i--;) {
        cout << "ON" << endl;
        arduino.pin(PIN).value(1);
        sleep(1);
        cout << "OFF" << endl;
        arduino.pin(PIN).value(0);
        sleep(1);
    }
    arduino.pin(IN).mode(mode::pullup_in);
    timer.async_wait(timer_handler);
    /*
    while (1) {
        cout << arduino.pin(IN).state() << endl;
        sleep(1);
    }
    */
    timer.expires_after(boost::asio::chrono::seconds(1));
    ioc.run();
    cout << "main: end" << endl;
    return 0;
}
